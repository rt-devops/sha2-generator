# sha256-generator

The service provides a simple REST API to generate SHA256 hashes based on user input. Project is based on [go-swagger](https://github.com/go-swagger/go-swagger) (OpenAPI v2.0). Business logic is located under _internal/_ folder.

This software is provided "as is" without warranty of any kind.

## Quickstart

Use _docker-compose_. It will build and run go binary for you as well as a tiny Postgres installation. 

```
$ rm -rf docker/data/db/.keep && docker-compose -f docker/docker-compose.yml up -d
```

Recommended versions: Docker 19.03.2, docker-compose 1.24.1. Tested under macOS 10.14.6 (Mojave)

### Verify the server

**Add new calculation task**

```
$ curl -X POST  --compressed -H 'Content-Type: application/json' -H 'Accept-Encoding: gzip' -d '{"payload": "1","hashRoundsCnt": 1}' 'http://127.0.0.1:8080/v1/task/create'
```

Expected output: 

```
{"hashRoundsCnt":1,"id":1,"payload":"1"}
```

**Check calculation task status**

```
$ curl -X GET  --compressed -H 'Content-Type: application/json' -H 'Accept-Encoding: gzip' 'http://127.0.0.1:8080/v1/task/1'
```

Use _id_ of the task received in first step in the URL _/task/{id}_. Expected output:

```
{"hash":"6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b","hashRoundsCnt":1,"id":1,"payload":"1","status":"Finished"}
```

## API

| Endpoint       | Method | Auth? | Example Payload                    | Description                     |
| -------------- | ------ | ----- | ---------------------------------- | ------------------------------- |
| `/task/create` | POST   | No    | {"payload": "1","hashRoundsCnt":1} | Schedule a new calculation task |
| `/task/{id}`   | GET    | No    | No                                 | Find task status by ID          |

## Advanced configuration

The service is supplied with reasonable defaults and ready to be started locally by docker-compose. Though it's possible to adjust some parameters.

*config.yml*

```
service:
  host: 0.0.0.0
  port: 8080
database:
  host: postgres
  port: 5432
  ssl: false # true | false
  name: sha2gen
  user: sha2gen
  password: password
```

In config file most options are self-explained. It stores database configuration and host/port the service needs to bind to. In _database_ section, _ssl_ option may be enabled to establish communication between the service and Postgres database over an encrypted channel (self-signed certificates are supplied with the service). In addition to this option, the corresponding line in _docker-compose.yml_ must be uncommented. These changes require Postgres restart and service's image rebuild.

*CLI flags*

```
$ sha2-generator -h
Usage:
  main [OPTIONS]

Application Options:
  -c, --config=  Path to the configuration file (default: config.yml) [$SHA2GEN_CONFIG_PATH]
  -t, --threads= Number of concurrent sha256 calculations allowed (default: 5) [$SHA2GEN_THREADS]
  -d, --debug    Debug mode [$SHA2GEN_DEBUG_MODE]

Help Options:
  -h, --help     Show this help message
```

The number of threads controls the number of simultaneous sha256 calculations the service performs in the background. API is designed in an asynchronous way - when service receives payload it returns a unique task ID and starts sha256 calculation internally. If the number of threads is significantly lower than the time service needs for one calculation, clients will have to wait for their task to complete longer. Too large threads number will lead to higher consumption of CPU and memory.
Debug mode enables verbose logging. 

## How to re-generate Swagger server code

```
$ swagger generate server -t $(pwd)/gen -f $(pwd)/swagger/swagger.yml  --template=stratoscale --exclude-main -A sha2-generator
```

## Throughput limits

- Max payload length = 1000000 (1 million) characters
- Max hash rounds = 1000000000 (1 billion)
  Not tested under larger numbers.

## Tests

Handlers and helper functions are mostly covered.
```
$ cd internal/task && go test -cover fmt
ok  	fmt	0.062s	coverage: 94.7% of statements
```
Main still needs to be covered. PRs are welcomed.

## TODO

- Write more tests
- Handle gzipped requests
- Simplify logging
- Consider cache layer
- CI using Bitbucket Pipelines

## License

MIT