CREATE TABLE IF NOT EXISTS hashes (
  id              SERIAL PRIMARY KEY,
  vc_payload      TEXT UNIQUE NOT NULL,
  vc_payload_hash VARCHAR(256) NOT NULL
);

CREATE TABLE IF NOT EXISTS requests (
  id                SERIAL PRIMARY KEY,
  vc_payload        TEXT NOT NULL,
  n_hash_rounds_cnt INT NOT NULL,
  vc_payload_hash   VARCHAR(256),
  n_time            INT NOT NULL,
  n_status_id       INT NOT NULL
);

CREATE TABLE IF NOT EXISTS statuses (
  id          SERIAL PRIMARY KEY,
  n_status_id INT UNIQUE NOT NULL,
  vc_name     VARCHAR(32) NOT NULL
);

ALTER TABLE requests ADD FOREIGN KEY (n_status_id) REFERENCES statuses (n_status_id);

INSERT INTO statuses (n_status_id, vc_name)
VALUES
  (1, 'In Progress'),
  (2, 'Finished')
ON CONFLICT DO NOTHING;