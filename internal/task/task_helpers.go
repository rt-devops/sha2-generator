package task

import (
	"database/sql"
	"fmt"
	"time"

	"bitbucket.org/rt-devops/sha2-generator/gen/models"
	log "github.com/go-pkgz/lgr"
	"github.com/minio/sha256-simd"
)

type SHA interface {
	CalcSHA256(db DB, sem chan struct{}, r *models.Task)
}

type DB interface {
	getHashFromDB(payload string) (string, error)
	saveHashToDB(payload, payloadHash string) error
	updateRequestHash(requestID int64, payloadHash string) error
}

func (t *Task) addTaskHelper(params *models.Task) (int, error) {
	if err := addTaskValidateParams(params); err != nil {
		return 422, err
	}

	var requestID sql.NullInt64

	time := time.Now().Unix()
	stmt := `INSERT INTO requests (vc_payload, n_hash_rounds_cnt, n_time, n_status_id)
	         VALUES ($1, $2, $3, $4)
					 RETURNING id`

	// insert new request to the database and get auto-incremented ID
	row := t.DB.QueryRow(stmt, params.Payload, params.HashRoundsCnt, time, requestStateInProgressID)
	switch err := row.Scan(&requestID); err {
	case sql.ErrNoRows:
		return 500, fmt.Errorf("Failed to add task to the queue")
	case nil:
		if requestID.Int64 <= 0 {
			return 500, fmt.Errorf("RequestID wasn't generated (requestID=%d)", requestID.Int64)
		}
		params.ID = requestID.Int64
		// send task to the queue which is processed by goroutines asynchronously
		t.Queue <- params
		log.Printf("DEBUG Task added to the queue (requestID=%d, payload=%s, hashRoundsCnt=%d)", requestID.Int64, params.Payload, params.HashRoundsCnt)
	default:
		return 500, fmt.Errorf("Unexpected error. Failed to add task to the queue (requestID=%d)", requestID.Int64)
	}
	return 200, nil
}

func (t *Task) getTaskStatusByIDHelper(id int64) (*models.Task, int, error) {
	var (
		requestID     sql.NullInt64
		payload       sql.NullString
		hashRoundsCnt sql.NullInt64
		statusName    sql.NullString
		payloadHash   sql.NullString
	)

	stmt := `SELECT r.id, s.vc_name, r.vc_payload, r.n_hash_rounds_cnt, r.vc_payload_hash
           FROM   requests r,
                  statuses s
           WHERE  r.id = $1
           AND    r.n_status_id = s.n_status_id;`

	row := t.DB.QueryRow(stmt, id)
	switch err := row.Scan(&requestID, &statusName, &payload, &hashRoundsCnt, &payloadHash); err {
	case sql.ErrNoRows:
		return nil, 500, fmt.Errorf("Failed to found request in the database")
	case nil:
		break
	default:
		return nil, 500, fmt.Errorf("Unexpected error. Failed to found request in the database")
	}

	return &models.Task{
		ID:            requestID.Int64,
		Status:        statusName.String,
		Payload:       payload.String,
		HashRoundsCnt: hashRoundsCnt.Int64,
		Hash:          payloadHash.String,
	}, 200, nil
}

func addTaskValidateParams(task *models.Task) error {
	// TODO: consider moving to middleware chain
	if len(task.Payload) == 0 || len(task.Payload) > maxPayload {
		return fmt.Errorf("Payload either too large or too small (min = 1, max = %d characters)", maxPayload)
	}
	if task.HashRoundsCnt > maxHashRoundsCnt || task.HashRoundsCnt < 1 {
		return fmt.Errorf("HashRoundsCnt either too large or too small (min = 1, max = %d rounds)", maxHashRoundsCnt)
	}
	return nil
}

func (t *Task) CalcScheduler(limit int, s SHA) {
	sem := make(chan struct{}, limit)
	for task := range t.Queue {
		sem <- struct{}{}
		log.Printf("DEBUG Calculation started (requestID=%d, payload=%s)", task.ID, task.Payload)
		go s.CalcSHA256(t, sem, task)
	}
}

func (t *Task) CalcSHA256(db DB, sem chan struct{}, r *models.Task) {
	defer func() { <-sem }()
	payload, hashRoundsCnt, requestID := r.Payload, r.HashRoundsCnt, r.ID

	// try to get first hash from db
	payloadHash, err := db.getHashFromDB(payload)
	if err != nil {
		log.Printf("ERROR Failed to get hash from the database, %s (payload=%s, payloadHash=%s hashRoundsCnt=%d)", err, payload, payloadHash, hashRoundsCnt)
	}
	log.Printf("DEBUG First hash found in the database (payload=%s, payloadHash=%s hashRoundsCnt=%d)", payload, payloadHash, hashRoundsCnt)

	hashRoundsCnt = hashRoundsCnt - 1
	if len(payloadHash) == 0 {
		log.Printf("DEBUG First hash isn't found in the database (payload=%s, payloadHash=%s)", payload, payloadHash)
		payloadHash, hashRoundsCnt = payload, hashRoundsCnt+1
	}

	// start sha256 calculation in a loop
	for hashRoundsCnt > 0 {
		shaWriter := sha256.New()
		_, err := shaWriter.Write([]byte(payloadHash))
		if err != nil {
			log.Printf("ERROR Failed to calculate sha256, %s (requestID=%d, payload=%s, hashRoundsCnt=%d)", err, requestID, payload, hashRoundsCnt)
			return
			// TODO: update request status in the database to 'Failed'
		}
		res := fmt.Sprintf("%x", shaWriter.Sum(nil))
		// keep in `hashes` table only the first hash since it's the most time-consuming to calculate for large inputs
		if payloadHash == payload {
			if err := db.saveHashToDB(payload, res); err != nil {
				log.Printf("ERROR Failed to save calculated hash in the database, %s (requestID=%d, payload=%s, hashRoundsCnt=%d, payloadHash=%v)", err, requestID, payload, hashRoundsCnt, payloadHash)
			}
		}
		payloadHash, hashRoundsCnt = res, hashRoundsCnt-1
	}
	r.Hash = payloadHash
	log.Printf("DEBUG Hash was generated successfully (requestID=%d, payload=%s, hashRoundsCnt=%d, payloadHash=%v)", requestID, payload, hashRoundsCnt, payloadHash)
	if err := db.updateRequestHash(requestID, payloadHash); err != nil {
		log.Printf("ERROR Failed to append calculated hash to the task, %s (requestID=%d, payload=%s, hashRoundsCnt=%d, payloadHash=%v)", err, requestID, payload, hashRoundsCnt, payloadHash)
		return
	}
}

func (t *Task) getHashFromDB(payload string) (string, error) {
	var hash string

	stmt := `SELECT vc_payload_hash FROM hashes WHERE vc_payload = $1`
	err := t.DB.QueryRow(stmt, payload).Scan(&hash)
	if err == sql.ErrNoRows {
		err = nil
	}
	return hash, err
}

func (t *Task) saveHashToDB(payload, payloadHash string) error {
	stmt := `INSERT INTO hashes (vc_payload, vc_payload_hash) VALUES ($1, $2)`
	_, err := t.DB.Exec(stmt, payload, payloadHash)
	return err
}

func (t *Task) updateRequestHash(requestID int64, payloadHash string) error {
	stmt := `UPDATE requests SET vc_payload_hash = $1, n_status_id = $2 WHERE id = $3`
	_, err := t.DB.Exec(stmt, payloadHash, requestStateFinishedID, requestID)
	return err
}
