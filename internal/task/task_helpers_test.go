package task

import (
	"testing"
	"time"

	"bitbucket.org/rt-devops/sha2-generator/gen/models"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/dchest/uniuri"
	"github.com/stretchr/testify/assert"
)

var tt = &Task{
	Threads: 5,
	Queue:   make(chan *models.Task, 1000),
}

func initDBMock(t *testing.T) sqlmock.Sqlmock {
	db, mock, err := sqlmock.New()

	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}
	tt.DB = db
	return mock
}

func TestAddTaskHelper(t *testing.T) {
	mock := initDBMock(t)
	defer tt.DB.Close()

	demoData := &models.Task{
		Payload:       "test-payload",
		HashRoundsCnt: 1,
	}

	// TODO: make a look instead of code duplication

	t.Logf("Test case: DB returned correctly generated requestID > 0")
	stmt := `INSERT INTO requests (.+)`
	result := mock.NewRows([]string{"id"}).AddRow(1)
	mock.ExpectQuery(stmt).
		WithArgs(demoData.Payload, demoData.HashRoundsCnt, time.Now().Unix(), 1).
		WillReturnRows(result)

	code, err := tt.addTaskHelper(demoData)
	assert.Equal(t, 200, code)
	assert.Equal(t, nil, err)

	t.Logf("Test case: DB returned correctly generated requestID = 0")
	result = mock.NewRows([]string{"id"}).AddRow(0)
	mock.ExpectQuery(stmt).
		WithArgs(demoData.Payload, demoData.HashRoundsCnt, time.Now().Unix(), 1).
		WillReturnRows(result)

	code, err = tt.addTaskHelper(demoData)
	assert.Equal(t, 500, code)
	assert.NotNil(t, err)

	t.Logf("Test case: DB returned correctly generated requestID = -1")
	result = mock.NewRows([]string{"id"}).AddRow(-1)
	mock.ExpectQuery(stmt).
		WithArgs(demoData.Payload, demoData.HashRoundsCnt, time.Now().Unix(), 1).
		WillReturnRows(result)

	code, err = tt.addTaskHelper(demoData)
	assert.Equal(t, 500, code)
	assert.NotNil(t, err)

	t.Logf("Test case: DB returned correctly generated requestID = NULL")
	mock.ExpectQuery(stmt).
		WithArgs(demoData.Payload, demoData.HashRoundsCnt, time.Now().Unix(), 1)

	code, err = tt.addTaskHelper(demoData)
	assert.Equal(t, 500, code)
	assert.NotNil(t, err)
}

func TestGetTaskStatusByID(t *testing.T) {
	mock := initDBMock(t)
	defer tt.DB.Close()

	demoData := &models.Task{
		ID:            1,
		Payload:       "test-payload",
		HashRoundsCnt: 1,
		Status:        "In Progress",
		Hash:          "6f06dd0e26608013eff30bb1e951cda7de3fdd9e78e907470e0dd5c0ed25e273",
	}

	testIDs := []int{1, 2}

	stmt := "SELECT (.+) FROM requests r, statuses s WHERE (.+)"
	for id := range testIDs {
		result := mock.NewRows([]string{"id", "vc_name", "vc_payload", "n_hash_rounds_cnt", "vc_payload_hash"}).AddRow(demoData.ID, demoData.Status, demoData.Payload, demoData.HashRoundsCnt, demoData.Hash)
		mock.ExpectQuery(stmt).
			WithArgs(demoData.ID).
			WillReturnRows(result)

		returrnedTask, code, err := tt.getTaskStatusByIDHelper(int64(id))

		if int64(id) == demoData.ID {
			t.Logf("Test case: passed ID found in the database")
			assert.Equal(t, demoData, returrnedTask)
			assert.Equal(t, 200, code)
			assert.Nil(t, err)
			return
		}
		t.Logf("Test case: passed ID not found in the database")
		assert.NotEqual(t, demoData, returrnedTask)
		assert.Equal(t, 500, code)
		assert.NotNil(t, err)
	}
}

func TestAddTaskValidateParams(t *testing.T) {
	type test struct {
		task *models.Task
		err  bool
	}
	var testCases = []test{
		test{
			// test case: all good
			task: &models.Task{
				Payload:       "test-payload",
				HashRoundsCnt: 1,
			},
			err: false,
		},
		test{
			// test case: all good, HashRoundsCnt = maxHashRoundsCnt
			task: &models.Task{
				Payload:       "test-payload",
				HashRoundsCnt: maxHashRoundsCnt,
			},
			err: false,
		},
		test{
			// test case: all good, len(payload) = maxPayload
			task: &models.Task{
				Payload:       uniuri.NewLen(maxPayload),
				HashRoundsCnt: 1,
			},
			err: false,
		},
		test{
			// test case: fail, no payload
			task: &models.Task{
				Payload:       "",
				HashRoundsCnt: 1,
			},
			err: true,
		},
		test{
			// test case: fail, HashRoundsCnt = -1
			task: &models.Task{
				Payload:       "test-payload",
				HashRoundsCnt: -1,
			},
			err: true,
		},
		test{
			// test case: fail, HashRoundsCnt = 0
			task: &models.Task{
				Payload:       "test-payload",
				HashRoundsCnt: 0,
			},
			err: true,
		},
		test{
			// test case: fail, HashRoundsCnt = maxHashRoundsCnt + 1
			task: &models.Task{
				Payload:       "test-payload",
				HashRoundsCnt: maxHashRoundsCnt + 1,
			},
			err: true,
		},
		test{
			// test case: fail, payload = maxPayload + 1
			task: &models.Task{
				Payload:       uniuri.NewLen(maxPayload + 1),
				HashRoundsCnt: 1,
			},
			err: true,
		},
	}

	for _, testCase := range testCases {
		err := addTaskValidateParams(testCase.task)
		if testCase.err {
			assert.NotNil(t, err)
		} else {
			assert.Nil(t, err)
		}
	}
}

type mockObj struct {
}

func (m mockObj) CalcSHA256(db DB, sem chan struct{}, r *models.Task) {
	func() { <-sem }()
}

func TestCalcScheduler(t *testing.T) {
	demoData := &models.Task{
		ID:            1,
		Payload:       "test-payload",
		HashRoundsCnt: 1,
		Status:        "In Progress",
	}

	for len(tt.Queue) > 0 {
		<-tt.Queue
	}

	tt.Queue <- demoData
	assert.Equal(t, 1, len(tt.Queue))
	close(tt.Queue)

	var mock = mockObj{}
	tt.CalcScheduler(5, mock)

	assert.Equal(t, 0, len(tt.Queue))
}

func (m mockObj) getHashFromDB(payload string) (string, error) {
	return "", nil
}

func (m mockObj) saveHashToDB(payload, payloadHash string) error {
	return nil
}

func (m mockObj) updateRequestHash(requestID int64, payloadHash string) error {
	return nil
}

func TestCalcSHA256(t *testing.T) {
	type test struct {
		task *models.Task
		hash string
	}
	var testCases = []test{
		test{
			task: &models.Task{
				Payload:       "test-payload",
				HashRoundsCnt: 1,
			},
			hash: "6f06dd0e26608013eff30bb1e951cda7de3fdd9e78e907470e0dd5c0ed25e273",
		},
		test{
			task: &models.Task{
				Payload:       "test-payload",
				HashRoundsCnt: 2,
			},
			hash: "668774e6bee7fe437eb921b4507d024b95a7aba97c32fc86527378d820727e45",
		},
	}
	var mock = mockObj{}
	ch := make(chan struct{}, 5)

	for _, test := range testCases {
		ch <- struct{}{}
		tt.CalcSHA256(mock, ch, test.task)
		assert.Equal(t, len(ch), 0)
		assert.Equal(t, test.hash, test.task.Hash)
	}
}

func TestGetHashFromDB(t *testing.T) {
	mock := initDBMock(t)
	defer tt.DB.Close()

	type test struct {
		task *models.Task
	}
	var testCases = []test{
		test{
			task: &models.Task{
				Payload:       "test-payload",
				HashRoundsCnt: 1,
				Hash:          "6f06dd0e26608013eff30bb1e951cda7de3fdd9e78e907470e0dd5c0ed25e273",
			},
		},
		test{
			task: &models.Task{
				Payload:       "test-payload",
				HashRoundsCnt: 2,
				Hash:          "668774e6bee7fe437eb921b4507d024b95a7aba97c32fc86527378d820727e45",
			},
		},
		test{
			task: &models.Task{
				Payload:       "",
				HashRoundsCnt: 2,
				Hash:          "",
			},
		},
	}

	stmt := "SELECT vc_payload_hash FROM hashes WHERE (.+)"
	for _, test := range testCases {
		result := mock.NewRows([]string{"vc_payload_hash"}).AddRow(test.task.Hash)
		mock.ExpectQuery(stmt).
			WithArgs(test.task.Payload).
			WillReturnRows(result)

		hash, err := tt.getHashFromDB(test.task.Payload)
		assert.Equal(t, test.task.Hash, hash)
		assert.Nil(t, err)
	}
}

func TestSaveHashToDB(t *testing.T) {
	mock := initDBMock(t)
	defer tt.DB.Close()

	type test struct {
		task *models.Task
		err  bool
	}
	var testCases = []test{
		test{
			task: &models.Task{
				Payload: "test-payload",
				Hash:    "6f06dd0e26608013eff30bb1e951cda7de3fdd9e78e907470e0dd5c0ed25e273",
			},
			err: false,
		},
		test{
			task: &models.Task{
				Payload: "test-payload",
				Hash:    "668774e6bee7fe437eb921b4507d024b95a7aba97c32fc86527378d820727e45",
			},
			err: false,
		},
	}

	stmt := `INSERT INTO hashes (.+)`

	for _, test := range testCases {
		mock.ExpectExec(stmt).
			WithArgs(test.task.Payload, test.task.Hash).WillReturnResult(sqlmock.NewResult(1, 1))

		err := tt.saveHashToDB(test.task.Payload, test.task.Hash)
		if test.err {
			assert.NotNil(t, err)
			continue
		}
		assert.Nil(t, err)
	}
}

func TestUpdateRequestHash(t *testing.T) {
	mock := initDBMock(t)
	defer tt.DB.Close()

	type test struct {
		task *models.Task
		err  bool
	}
	var testCases = []test{
		test{
			task: &models.Task{
				ID:   1,
				Hash: "6f06dd0e26608013eff30bb1e951cda7de3fdd9e78e907470e0dd5c0ed25e273",
			},
			err: false,
		},
	}

	stmt := `UPDATE requests SET (.+)`

	for _, test := range testCases {
		mock.ExpectExec(stmt).
			WithArgs(test.task.Hash, 2, test.task.ID).WillReturnResult(sqlmock.NewResult(1, 1))

		err := tt.updateRequestHash(test.task.ID, test.task.Hash)
		if test.err {
			assert.NotNil(t, err)
			continue
		}
		assert.Nil(t, err)
	}
}
