package task

import (
	"context"
	"database/sql"

	"bitbucket.org/rt-devops/sha2-generator/gen/models"
	"bitbucket.org/rt-devops/sha2-generator/gen/restapi/operations/task"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/swag"
	log "github.com/go-pkgz/lgr"
)

const (
	requestStateInProgressID = 1
	requestStateFinishedID   = 2
	// app limits
	maxPayload       = 1000000
	maxHashRoundsCnt = 1000000000
)

type Task struct {
	DB      *sql.DB
	Threads int
	Queue   chan *models.Task
}

func (t *Task) AddTask(ctx context.Context, params task.AddTaskParams) middleware.Responder {
	code, err := t.addTaskHelper(params.Task)
	if err != nil {
		log.Printf("ERROR %s (payload=%s, hashRoundsCnt=%d)", err, params.Task.Payload, params.Task.HashRoundsCnt)
		return task.NewAddTaskDefault(code).WithPayload(&models.Error{Message: swag.String(err.Error())})
	}
	return task.NewAddTaskOK().WithPayload(params.Task)
}

func (t *Task) GetTaskStatusByID(ctx context.Context, params task.GetTaskStatusByIDParams) middleware.Responder {
	response, code, err := t.getTaskStatusByIDHelper(params.ID)
	if err != nil {
		return task.NewGetTaskStatusByIDDefault(code).WithPayload(&models.Error{Message: swag.String(err.Error())})
	}
	return task.NewGetTaskStatusByIDOK().WithPayload(response)
}
