package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/rt-devops/sha2-generator/gen/models"
	"bitbucket.org/rt-devops/sha2-generator/gen/restapi"
	"bitbucket.org/rt-devops/sha2-generator/internal/task"
	log "github.com/go-pkgz/lgr"
	"github.com/gorilla/handlers"
	flag "github.com/jessevdk/go-flags"
	"github.com/justinas/alice"
	_ "github.com/lib/pq"
	yaml "gopkg.in/yaml.v2"
)

var (
	commit  string
	builtAt string
	builtBy string
	builtOn string
	opts    struct {
		ConfPath string `short:"c" long:"config" description:"Path to the configuration file" default:"config.yml" env:"SHA2GEN_CONFIG_PATH"`
		Threads  int    `short:"t" long:"threads" description:"Number of concurrent sha256 calculations allowed" default:"5" env:"SHA2GEN_THREADS"`
		Dbg      bool   `short:"d" long:"debug" description:"Debug mode" env:"SHA2GEN_DEBUG_MODE"`
	}
)

type Config struct {
	Service struct {
		Host string `yaml:"host"`
		Port int    `yaml:"port"`
	} `yaml:"service"`
	Database struct {
		Host     string `yaml:"host"`
		Port     int    `yaml:"port"`
		SSL      bool   `yaml:"ssl"`
		Name     string `yaml:"name"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
	} `yaml:"database"`
}

func main() {
	if _, err := flag.Parse(&opts); err != nil {
		os.Exit(0)
	}
	setupLog(opts.Dbg)

	log.Printf("INFO sha2-generator | commit: %s | built at %s by %s on %s\n", commit, builtAt, builtBy, builtOn)

	conf := parseConfig(opts.ConfPath)
	db := initDB(conf)
	defer db.Close()

	t := task.Task{
		DB:      db,
		Threads: opts.Threads,
		Queue:   make(chan *models.Task),
	}

	// start manager which controls workers performing sha256 calculation
	go t.CalcScheduler(opts.Threads, &t)

	// chain http middleware
	commonMiddleware := alice.New(logMiddleware, handlers.CompressHandler)
	// initiate the http handler, with the objects that are implementing the business logic
	h, err := restapi.Handler(restapi.Config{
		TaskAPI:         &t,
		Logger:          log.Printf,
		InnerMiddleware: func(h http.Handler) http.Handler { return commonMiddleware.Then(h) },
	})
	if err != nil {
		log.Printf("FATAL %s", err)
	}

	log.Printf("INFO Starting to serve, access server on http://%s:%d", conf.Service.Host, conf.Service.Port)
	if err := http.ListenAndServe(fmt.Sprintf("%s:%d", conf.Service.Host, conf.Service.Port), h); err != nil {
		log.Printf("FATAL %s", err)
	}
}

func parseConfig(path string) *Config {
	conf := &Config{}
	data, err := ioutil.ReadFile(filepath.Clean(path))
	if err != nil {
		log.Printf("FATAL %s", err)
	}
	if err := yaml.Unmarshal(data, conf); err != nil {
		log.Printf("FATAL %s", err)
	}
	return conf
}

func initDB(conf *Config) *sql.DB {
	sslMode := "disable"
	if conf.Database.SSL {
		sslMode = "require"
	}
	connStr := fmt.Sprintf("host=%s port=%d dbname=%s user=%s password='%s' sslmode=%s", conf.Database.Host, conf.Database.Port, conf.Database.Name, conf.Database.User, conf.Database.Password, sslMode)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Printf("FATAL %s", err)
	}

	// waiting for db in indefinite loop
	for {
		if err := db.Ping(); err != nil {
			log.Printf("ERROR Database isn't responding, %s", err)
			time.Sleep(1 * time.Second)
		} else {
			log.Printf("INFO Database connection established")
			return db
		}
	}
}

func setupLog(dbg bool) {
	if dbg {
		log.Setup(log.Debug, log.CallerFile, log.CallerFunc, log.Msec, log.LevelBraces)
		return
	}
	log.Setup(log.Msec, log.LevelBraces)
}

var logMiddleware = func(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t1 := time.Now()
		h.ServeHTTP(w, r)
		method := r.Method
		path := r.URL.Path
		t2 := time.Now()
		log.Printf("INFO [%s] %s %.2fms", method, path, float64(t2.Sub(t1))/float64(time.Millisecond))
	})
}
