// Code generated by go-swagger; DO NOT EDIT.

/*
Package restapi SHA2 Generator
Provides simple REST API to generate SHA256 hashes based on user input.


    Schemes:
      http
    Host: localhost:8080
    BasePath: /v1
    Version: 0.0.1
    License: MIT
    Contact: <rt-devops@yandex.com>

    Consumes:
    - application/json

    Produces:
    - application/json

swagger:meta
*/
package restapi
