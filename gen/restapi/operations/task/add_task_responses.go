// Code generated by go-swagger; DO NOT EDIT.

package task

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	models "bitbucket.org/rt-devops/sha2-generator/gen/models"
)

// AddTaskOKCode is the HTTP code returned for type AddTaskOK
const AddTaskOKCode int = 200

/*AddTaskOK Task created successfully

swagger:response addTaskOK
*/
type AddTaskOK struct {

	/*
	  In: Body
	*/
	Payload *models.Task `json:"body,omitempty"`
}

// NewAddTaskOK creates AddTaskOK with default headers values
func NewAddTaskOK() *AddTaskOK {

	return &AddTaskOK{}
}

// WithPayload adds the payload to the add task o k response
func (o *AddTaskOK) WithPayload(payload *models.Task) *AddTaskOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the add task o k response
func (o *AddTaskOK) SetPayload(payload *models.Task) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *AddTaskOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

/*AddTaskDefault Generic error

swagger:response addTaskDefault
*/
type AddTaskDefault struct {
	_statusCode int

	/*
	  In: Body
	*/
	Payload *models.Error `json:"body,omitempty"`
}

// NewAddTaskDefault creates AddTaskDefault with default headers values
func NewAddTaskDefault(code int) *AddTaskDefault {
	if code <= 0 {
		code = 500
	}

	return &AddTaskDefault{
		_statusCode: code,
	}
}

// WithStatusCode adds the status to the add task default response
func (o *AddTaskDefault) WithStatusCode(code int) *AddTaskDefault {
	o._statusCode = code
	return o
}

// SetStatusCode sets the status to the add task default response
func (o *AddTaskDefault) SetStatusCode(code int) {
	o._statusCode = code
}

// WithPayload adds the payload to the add task default response
func (o *AddTaskDefault) WithPayload(payload *models.Error) *AddTaskDefault {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the add task default response
func (o *AddTaskDefault) SetPayload(payload *models.Error) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *AddTaskDefault) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(o._statusCode)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}
